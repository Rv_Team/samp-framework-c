#include "main.h"
#include <Windows.h>
#include <cmath>
#include "receivepacket.cpp"
#include "Source.cpp"


SAMPFramework *pSAMP;
CD3DHook *pD3DHook;
RakClientInterface	*pRakNet;
HookedRakClientInterface *pHook;


enum DATA_RADAR {
	playerid,
	packet_id,
};

float flt = 0.000001f;


void InjectJmp(unsigned long _offset, void* target) {

	void* pBlock = VirtualAlloc(0, 0x1000, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	unsigned long Protection;
	VirtualProtect((void*)_offset, 5, PAGE_EXECUTE_READWRITE, &Protection);
	unsigned long offs = (unsigned long)pBlock - (_offset + 5);
	*reinterpret_cast<unsigned char*>(_offset) = 0xE9;
	memcpy((LPVOID)(_offset + 1), &offs, 4);
	VirtualProtect((void*)_offset, 5, Protection, 0);

	memcpy(pBlock, "\xE9", 1);
	offs = (DWORD)target - ((DWORD)pBlock + 5);
	memcpy((void*)((unsigned long)pBlock + 1), &offs, sizeof(void*));
	memcpy((void*)((unsigned long)pBlock + 5), "\xE9", 1);
	unsigned long retAddr = ((DWORD)pBlock + 5 + 5) - 0x58334D;
	memcpy((void*)((unsigned long)pBlock + 6), &retAddr, sizeof(retAddr));
}

struct vec { float x, y; };
float __cdecl LimitRadarPoint(vec* point) {

	if (*reinterpret_cast<unsigned char*>(0xBA67A1)) {
		return (float)sqrt(point->y * point->y + point->x * point->x);
	}
	float sr = sqrt(point->y * point->y + point->x * point->x);

	float x;
	if (sr > 1.0)
	{
		if (point->x > -1.0 && point->x < 1.0 && point->y > -1.0 && point->y < 1.0)
			return 6.0f; // 0.99

		float y = atan2(point->y, point->x);

		float angle = y * 70.295779513f; // 57
		if (angle > 45.0 || angle <= -45.0)
		{
			if (angle > 45.0 && angle <= 135.0)
			{
				point->x = cos(angle / 70.295779513f) * 2.4142135623f; // 1
				point->y = 2.0; // 1
				
				return sr;
			}
			if (angle <= 135.0 && angle > -135.0)
			{
				point->x = cos(angle / 70.295779513f) * 2.4142135623f; // 1
				point->y = -2.0; // -1
				return sr;
			}
			x = -2.0; // -1
		}
		else
		{
			x = 2.0; //1
		}
		point->x = x;
		point->y = sin(angle / 70.295779513f) * 2.4142135623f; // 1
	}
	return sr;

}

template<typename T>
void WriteMemory(void* addr, T value) {
	DWORD oldProt = 0;
	VirtualProtect(addr, sizeof(T), PAGE_EXECUTE_READWRITE, &oldProt);
	*reinterpret_cast<T*>(addr) = value;
	VirtualProtect(addr, sizeof(T), oldProt, NULL);
}


void mainThread(void *pvParams)
{
	if (pSAMP)
	{
		while (!pSAMP->tryInit())
			Sleep(100);


		while (!pSAMP->isInited)
			continue;

		//strcpy(pSAMP->getInfo()->szIP, "127.0.0.1");
		//pSAMP->getInfo()->ulPort = 7777;
	
	}
}
DWORD FindAddress(HMODULE hModule, const char* mem_tofind, size_t strSize) {
	MODULEINFO moduleInfo;
	DWORD dwModule = reinterpret_cast<DWORD>(hModule);
	GetModuleInformation(GetCurrentProcess(), hModule, &moduleInfo, sizeof(moduleInfo));
	for (auto i = dwModule; i < moduleInfo.SizeOfImage + dwModule - 1 - strSize; i++) {
		if (memcmp(reinterpret_cast<void*>(i), mem_tofind, strSize) == 0) {
			return i;
		}
	}
	return 0;
}
bool OnReceivePacket(Packet* p)
{
	if (p->data == nullptr || p->length == 0)
		return true;
	if (p->data[0] == PACKET_ID_CUSTOM) // packetId
	{
		//работа с пакетами
	}
	return true;
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReasonForCall, LPVOID lpReserved)
{
	switch (dwReasonForCall)
	{
		case DLL_PROCESS_ATTACH:
		{
			pSAMP = new SAMPFramework(GetModuleHandle("samp.dll"));
			_beginthread(mainThread, NULL, NULL);
			pD3DHook = new CD3DHook();
			break;
		}
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
	}
	return TRUE;
}

